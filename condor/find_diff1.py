import argparse
import os
import sys
sys.path.append('.')


local = False

def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def main(params):

    src_root = params.src_root
    output_root = params.output_root

    diff_files = []

    for root, _, files in os.walk(src_root):
        for name in files:
            if name.endswith(".openpose.tar.xz"):

                if name.find("+") != -1:  # skip file_name with '+'
                    continue
                seq_name = name.split('.')[0]
                src_file = os.path.join(root, name)

                relative_dir = os.path.relpath(src_file, src_root)
                sub_dir = os.path.join(*relative_dir.split('/')[:-1])
                output_dir = os.path.join(output_root, sub_dir)
                output_path = os.path.join(output_dir, "{}_Czech.gklz".format(seq_name))

                if os.path.exists(output_path):
                    continue

                diff_files.append(os.path.join(root,name))


    # diff_files = [
    #     os.path.join(root, name)
    #     for root, _, files in os.walk(src_root)
    #     for name in files
    #     if name.endswith(".eaf") and not os.path.exists(os.path.join(output_root, os.path.relpath(os.path.join(root, name), src_root)).replace('.eaf', '.gklz'))
    # ]

    #print(diff_files)
    print(*diff_files, sep="\n")
    pass




if __name__ == "__main__":

    # load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--src_root",
        type=str,
        default='/vol/research/extol/data/BSLCP/Processed.OpenPose.surrey/',
    )


    parser.add_argument(
        "--output_root",
        type=str,
        default='/vol/research/SignTranslation/tmp/tj0012/BSLCP/Czech'
    )
    params, _ = parser.parse_known_args()

    main(params)