
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import skeletalModel
import numpy as np
import os
save_fig = True
from utils import make_dir



def drawJoints(Yx, Yy, Yz, root_dir, folder_name):
    plt.ion()
    plt.show()
    fig = plt.figure(figsize=(7.2, 7.2))

    save_folder = ''
    ax = fig.add_subplot(111,projection='3d')

    ax.view_init(-90, -90) # tj 90, 90 front
    save_folder = os.path.join(root_dir, folder_name)
    make_dir(save_folder)
    skeleton = skeletalModel.getSkeletalModelStructure()

    T, n = Yx.shape

    skeleton = np.array(skeleton)

    number = skeleton.shape[0]
    cmap = plt.get_cmap('rainbow')
    colors = [cmap(i) for i in np.linspace(0, 1, number)]
    plt.axis('off')

    for i in range(T):
        # for j in range(n):
        #      #print(Yx[i,j], Yy[i,j], Yz[i,j])
        #      ax.scatter(Yx[i,j], Yy[i,j], Yz[i,j], c = 'b', marker = 'o')

        for j in range(number):
            ax.plot([Yx[i,skeleton[j,0]], Yx[i,skeleton[j,1]]], [Yy[i,skeleton[j,0]], Yy[i,skeleton[j,1]]], [Yz[i,skeleton[j,0]], Yz[i,skeleton[j,1]]], color=colors[j])
        #plt.draw()
        if save_fig:
            plt.savefig(os.path.join(save_folder, "%08d.png" % i), dpi=100)
        plt.show()
        plt.pause(0.3)
        for j in range(number):
            ax.lines.pop(0)
        #plt.clf()



