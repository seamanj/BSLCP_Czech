import argparse
import os
import numpy as np


import skeletalModel
import pose2D
import pose2Dto3D
import pose3D

# import draw

import _pickle as cPickle
import gzip

import re
import tarfile
import json
from utils import make_dir

def load_zipped_pickle(filename):
    with gzip.open(filename, 'rb') as f:
        loaded_object = cPickle.load(f)
        return loaded_object

def save_zipped_pickle(obj, filename, protocol=-1):
    with gzip.open(filename, 'wb') as f:
        cPickle.dump(obj, f, protocol)

def boolean_string(s):
    if s not in {'False', 'True'}:
        raise ValueError('Not a valid boolean string')
    return s == 'True'

def load_openpose_tar_xz(filename):
    keypoints = {}
    tar = tarfile.open(filename)
    for member in tar.getmembers():
        if member.isfile():
            filename = os.path.abspath(member.name)
            # print(filename)
            num_frame = re.findall('\d{12,}', filename)
            num_frame = num_frame[0].lstrip('0')  # tj : the first result
            if num_frame == '':
                num_frame = 0
            num_frame = int(num_frame)
            f = tar.extractfile(member)
            keypoints[num_frame] = json.loads(f.read().decode('utf-8'))
    return keypoints

def main(params):


    input_file = params.input_file
    input_root = params.input_root
    output_root = params.output_root
    file_name = input_file.split("/")[-1]
    parent_dir = input_file.split("/")[-2]
    sub_dir = input_file.replace(input_root, '')
    sub_dir = sub_dir.replace(file_name,'')

    print('file_name:{}'.format(file_name))
    if file_name.find("+") != -1: # skip file_name with '+'
        return
    seq_name = file_name.split('.')[0]

    output_dir = os.path.join(output_root, sub_dir)
    make_dir(output_dir)
    output_path = os.path.join(output_dir, "{}_Czech.gklz".format(seq_name))

    if os.path.exists(output_path):
        print("{} exists already".format(output_path))
        return

    openpose_root_dir = "/vol/research/extol/data/BSLCP/Processed.OpenPose.surrey"
    openpose_filename = os.path.join(openpose_root_dir, sub_dir, "%s.openpose.tar.xz" % seq_name)

    print("openpose_filename: %s"%openpose_filename)

    dtype = "float32"
    randomNubersGenerator = np.random.RandomState(1234)

    structure = skeletalModel.getSkeletalModelStructure()
    inputSequence_2D = []

    body_mapping_openpose_to_czech = [0, 1, 5, 6, 2, 3] # tj : remove 7 4

    assert os.path.exists(openpose_filename), '{} doesn\'t exist'.format(openpose_filename)
    keypoints = load_openpose_tar_xz(openpose_filename)
    num_frames = len(keypoints)

    for i in range(num_frames):
        num_people = len(keypoints[i]["people"])
        if num_people == 0:
            inputSequence_2D.append(np.zeros(3*48 + 1))# tj : 1 for success flag
            continue
        elif num_people > 1:
            dist_min = float('inf')
            x_shoulder_center_most_right = keypoints[i]["people"][0]["pose_keypoints_2d"][3] # tj : 3 for the x position of the shoulder center
            # = tj : the second [0] for x coordinate
            idx_people = 0
            for j in range(1, num_people):
                x_shoulder_center_curr = keypoints[i]["people"][j]["pose_keypoints_2d"][3]
                if x_shoulder_center_curr > x_shoulder_center_most_right:
                    x_shoulder_center_most_right = x_shoulder_center_curr
                    idx_people = j
        else:
            idx_people = 0

        # tj : extract the keypoints
        face_keypoints_2d = keypoints[i]["people"][idx_people]["face_keypoints_2d"]  # poses is 0-based
        face_keypoints_2d = np.reshape(face_keypoints_2d, (-1, 3))
        #
        pose_keypoints_2d = keypoints[i]["people"][idx_people]["pose_keypoints_2d"]
        pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))
        #
        hand_right_keypoints_2d = keypoints[i]["people"][idx_people]["hand_right_keypoints_2d"]
        hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))
        #
        hand_left_keypoints_2d = keypoints[i]["people"][idx_people]["hand_left_keypoints_2d"]
        hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))
        #
        frame_data = np.concatenate((pose_keypoints_2d[body_mapping_openpose_to_czech].flatten(),
                                    hand_right_keypoints_2d.flatten(),
                                    hand_left_keypoints_2d.flatten(), [1]), axis=0)
        # tj : not in our view, in the skeleton view to differentiate the left and the right
        #
        inputSequence_2D.append(frame_data)

    inputSequence_2D = np.array(inputSequence_2D)

    # Decomposition of the single matrix into three matrices: x, y, w (=likelihood)
    X = inputSequence_2D
    # tj : each row stands for a frame, in one row, data goes like : x1,y1,w1, x2,y2,w2
    Xx = X[0:X.shape[0], 0:(X.shape[1]):3]  # tj : (583, 50), jump by every 3 elements
    Xy = X[0:X.shape[0], 1:(X.shape[1]):3]  # tj : (583, 50)
    Xw = X[0:X.shape[0], 2:(X.shape[1]):3]  # tj : (583, 50)
    Xs = X[0:X.shape[0], -1] # tj : whether this frame is success

    Xw_bak = Xw

    # Normalization of the picture (x and y axis has the same scale)
    Xx, Xy = pose2D.normalization(Xx, Xy)
    # save("data/openpose/demo1.txt", [Xx, Xy, Xw])

    # Delete all skeletal models which have a lot of missing parts.
    Xx, Xy, Xw, Xs = pose2D.prune(Xx, Xy, Xw, Xs, (0, 1, 2, 3, 4, 5, 6, 7), 0.3, dtype)# make success 0 if frame is pruned
    # save("data/openpose/demo2.txt", [Xx, Xy, Xw])

    # Preliminary filtering: weighted linear interpolation of missing points.
    Xx, Xy, Xw = pose2D.interpolation(Xx, Xy, Xw, 0.99,
                                      dtype)  # tj : for one keypoint, if its confidence is below threshold, then do interpolation from the past and the future frames.
    # save("data/openpose/demo3.txt", [Xx, Xy, Xw])

    # Initial 3D pose estimation
    lines0, rootsx0, rootsy0, rootsz0, anglesx0, anglesy0, anglesz0, Yx0, Yy0, Yz0 = pose2Dto3D.initialization(
        Xx,
        Xy,
        Xw,
        structure,
        0.001,  # weight for adding noise
        randomNubersGenerator,
        dtype
    )
    print("step 4 is done!")

    Yx, Yy, Yz = pose3D.backpropagationBasedFiltering(
        lines0,
        rootsx0,
        rootsy0,
        rootsz0,
        anglesx0,
        anglesy0,
        anglesz0,
        Xx,
        Xy,
        Xw,
        structure,
        dtype,
    )
    print("step 5 is done!")





    save_data = {}
    save_data['seq_name'] = seq_name
    save_data['skeleton'] = np.dstack((Yx, Yy, Yz))
    save_data['confidence'] = Xw_bak
    save_data['success'] = Xs.astype(np.int)
    save_zipped_pickle(save_data, output_path)
    print("{}_Czech.gklz is saved".format(seq_name))
    if False:
        draw.drawJoints(Yx, Yy, Yz, output_dir, 'images')


def test():
    data = load_zipped_pickle("/vol/research/SignTranslation/tmp/tj0012/BSLCP/Czech/Interviews/Glasgow/25+26/G26i_Czech.gklz")
    skeleton = data['skeleton']
    output_dir = '/home/seamanj/test_results'
    make_dir(output_dir)
    import draw
    draw.drawJoints(skeleton[:,:,0], skeleton[:,:,1], skeleton[:,:,2], output_dir, 'images')
    print(1)

if __name__ == "__main__":

    # test()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_file",
        type=str,
        default="/vol/research/extol/data/BSLCP/Processed.OpenPose.surrey/Conversation/Belfast/27+28/BF27c.openpose.tar.xz",
        help="",
    )
    parser.add_argument(
        "--input_root",
        type=str,
        default="/vol/research/extol/data/BSLCP/Processed.OpenPose.surrey/",
        help="",
    )
    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/SignTranslation/tmp/tj0012/BSLCP/Czech",
        help="",
    )

    params, _ = parser.parse_known_args()

    main(params)


# tj : the following openpose files are missing.
#
# BL2i
# BL3i
# BL4i
# BL5i
# BL6i
# BL7i
# BL8i
# BL10i
# BL11i
# BL12i
# BL13i