#!/bin/bash

python=$1
full_path=$2

cd /vol/research/SignPose/tj/Workspace/BSLCP_Czech

$python main.py \
--input_file=$full_path \
--input_root=/vol/research/extol/data/BSLCP/Processed.OpenPose.surrey/ \
--output_root=/vol/research/SignTranslation/tmp/tj0012/BSLCP/Czech
